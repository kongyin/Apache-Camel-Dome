package com.yuepeng.camel.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName BeanProcessor
 * @Description TODO
 * @Author kongyin
 * @Date 2020/8/4 11:43
 **/
@Component("BeanProcessor")
public class BeanProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setBody("Apache Camel Mail 测试 --发送时间： "+new Date());
    }
}
