package com.yuepeng.camel.route;

import com.yuepeng.camel.processor.HttpProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyRouteBuilder
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/27 10:45
 **/
//@Component
public class MyRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {
        from("timer:timer?period={{interval:1000}}")
                .to("http4:?bridgeEndpoint=true&connectionClose=true")
                .process(new HttpProcessor());
    }
}
