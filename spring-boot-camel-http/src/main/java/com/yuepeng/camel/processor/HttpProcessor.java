package com.yuepeng.camel.processor;

import com.alibaba.fastjson.JSON;
import com.yuepeng.camel.pojo.Order;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

/**
 * @ClassName HttpProcessor
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/23 17:02
 **/
@Component
public class HttpProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        /*Order order = JSON.parseObject(body, Order.class);
        long l = System.currentTimeMillis();
        order.setId((int)( l % 100 ));
        exchange.getOut().setBody(order);*/
        System.out.println(body);
    }
}
