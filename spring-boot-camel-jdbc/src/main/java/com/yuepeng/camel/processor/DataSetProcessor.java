package com.yuepeng.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * @ClassName DataSetProcessor
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/21 14:47
 **/
public class DataSetProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        System.out.println("JDBC组件测试打印---->："+exchange.getIn().getBody(String.class));
    }
}
