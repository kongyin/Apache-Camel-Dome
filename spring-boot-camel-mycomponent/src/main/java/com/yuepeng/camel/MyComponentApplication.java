package com.yuepeng.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName MyComponentApplication
 * @Description TODO
 * @Author kongyin
 * @Date 2020/8/3 15:10
 **/
@SpringBootApplication
public class MyComponentApplication {
    public static void main(String[] args) {
        SpringApplication.run(MyComponentApplication.class,args);
    }
}

