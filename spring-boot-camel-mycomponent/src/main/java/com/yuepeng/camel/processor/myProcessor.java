package com.yuepeng.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

/**
 * @ClassName myProcessor
 * @Description TODO
 * @Author kongyin
 * @Date 2020/8/3 15:17
 **/
@Component("myProcessor")
public class myProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        System.out.println(body);
    }
}
