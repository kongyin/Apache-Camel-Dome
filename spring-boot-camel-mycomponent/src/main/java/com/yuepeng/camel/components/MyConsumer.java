package com.yuepeng.camel.components;

import org.apache.camel.*;
import org.apache.camel.impl.DefaultConsumer;
import org.apache.camel.spi.ShutdownAware;

public class MyConsumer extends DefaultConsumer implements ShutdownAware {

    private String name;

    public MyConsumer(MyEndpoint myEndpoint, Processor processor) {
        super(myEndpoint,processor);
        this.name = getName(myEndpoint);
    }

    private String getName(MyEndpoint myEndpoint) {
        return myEndpoint.getEndpointUri().substring(myEndpoint.getEndpointUri().indexOf(":")+1);
    }

    private void processExchange(Exchange exchange) throws Exception {
        this.getProcessor().process(exchange);
    }


    @Override
    public boolean deferShutdown(ShutdownRunningTask shutdownRunningTask) {
        return false;
    }

    @Override
    public int getPendingExchangesSize() {
        return 0;
    }

    @Override
    public void prepareShutdown(boolean suspendOnly, boolean forced) {
        MyEndpoint endpoint = (MyEndpoint) getEndpoint();
        Exchange exchange = endpoint.createExchange(name);
        try {
            processExchange(exchange);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
