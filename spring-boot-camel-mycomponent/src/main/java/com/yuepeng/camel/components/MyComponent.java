package com.yuepeng.camel.components;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("hello")
public class MyComponent extends DefaultComponent {

    @Override
    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
        return new MyEndpoint(this, uri);
    }


}
