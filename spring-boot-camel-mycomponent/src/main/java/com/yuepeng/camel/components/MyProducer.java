package com.yuepeng.camel.components;

import org.apache.camel.Exchange;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultProducer;

public class MyProducer extends DefaultProducer {

    private String name;

    public MyProducer(MyEndpoint myEndpoint) {
        super(myEndpoint);
        this.name = getName(myEndpoint);
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        if(body!=null) {
            exchange.getOut().setBody(name + " hello, this my component , body is 《" + body + " 》");
        }else {
            exchange.getOut().setBody(name + " hello, this my component , but not body");
        }
    }

    private String getName(MyEndpoint myEndpoint) {
        return myEndpoint.getEndpointUri().substring(myEndpoint.getEndpointUri().indexOf(":")+3);
    }
}
