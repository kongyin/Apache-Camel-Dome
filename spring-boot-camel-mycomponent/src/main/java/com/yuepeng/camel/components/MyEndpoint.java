package com.yuepeng.camel.components;

import org.apache.camel.*;
import org.apache.camel.impl.DefaultEndpoint;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.spi.Metadata;
import org.apache.camel.spi.UriEndpoint;
import org.apache.camel.spi.UriPath;


@UriEndpoint(firstVersion = "1.0.0", scheme = "hello", title = "Hello", syntax = "hello:name")
public class MyEndpoint extends DefaultEndpoint {
    public static final String HELLO_EXCHANGE_HELLO = "CamelHelloExchangeHello";

    //路径参数
    @UriPath(description = "Name of hello endpoint") @Metadata(required = "true")
    private String name;

    public MyEndpoint(MyComponent myComponent, String uri) {
        super(uri, myComponent);

    }

    @Override
    public Producer createProducer() throws Exception {
        return new MyProducer(this);
    }

    @Override
    public Consumer createConsumer(Processor processor) throws Exception {
        return new MyConsumer(this, processor);
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    public Exchange createExchange(String hello) {
        Exchange exchange = new DefaultExchange(getCamelContext());
        exchange.setProperty(HELLO_EXCHANGE_HELLO, hello);
        exchange.getIn().setBody(hello, String.class);
        return exchange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
