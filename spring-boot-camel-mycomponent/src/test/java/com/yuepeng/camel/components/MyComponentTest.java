package com.yuepeng.camel.components;


import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class MyComponentTest {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        CamelContext camelContext = new DefaultCamelContext();
        //为了简单起见，用该方法替代了在类路径META-INF/services/org/apache/camel/component/下放置properties文件方式
        camelContext.addComponent("hello", new MyComponent());

        camelContext.addRoutes(new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                this.from("timer:foo?period=2000&repeatCount=1")
                        .setBody(constant("我是body"))
                        .to("hello:kongyin")
                        .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        String body = exchange.getIn().getBody(String.class);
                        System.out.println(body);
                    }
                }).log("end ${body}");

            }
        });

        camelContext.start();

        Object object = new Object();
        synchronized (object) {
            object.wait();
        }
    }
}
