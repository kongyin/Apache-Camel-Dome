package com.yuepeng.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * @ClassName HttpProcessor
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/23 17:02
 **/
public class HttpProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        System.out.println("Http组件测试打印---->："+exchange.getIn().getBody(String.class));
    }
}
