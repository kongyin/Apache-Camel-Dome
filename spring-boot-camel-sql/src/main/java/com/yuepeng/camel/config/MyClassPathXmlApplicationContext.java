package com.yuepeng.camel.config;

import org.apache.camel.spring.boot.SpringBootXmlCamelContextConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


/**
 * @ClassName MyClassPathXmlApplicationContext
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/20 13:10
 **/
//@Component
public class MyClassPathXmlApplicationContext extends SpringBootXmlCamelContextConfigurer {

    @Bean
    public ClassPathXmlApplicationContext classPathXmlApplicationContext(){
        return new ClassPathXmlApplicationContext("spring/*.xml");
    }
}
