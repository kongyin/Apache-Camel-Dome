package com.yuepeng.camel.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

/**
 * @ClassName RestApi
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/23 14:08
 **/
@Component
public class RestApi extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        restConfiguration()
                .contextPath("/camel-rest-sql").apiContextPath("/api-doc")
                .apiProperty("api.title", "Camel REST API")
                .apiProperty("api.version", "1.0")
                .apiProperty("cors", "true")
                .apiContextRouteId("doc-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest("/books").description("Books REST service")
                .get("/").description("所有书籍清单")
                .route().routeId("books-api")
                .to("sql:select distinct description from orders?" +
                        "dataSource=#dataSource&" +
                        "outputClass=com.yuepeng.camel.pojo.Book")
                .endRest()
                .get("order/{id}").description("Details of an order by id")
                .route().routeId("order-api")
                .to("sql:select * from orders where id = :#${header.id}?" +
                        "dataSource=#dataSource&outputType=SelectOne&" +
                        "outputClass=com.yuepeng.camel.pojo.Order");
    }
}
