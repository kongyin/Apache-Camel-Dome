package com.yuepeng.camel.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * @ClassName Backend
 * @Description TODO
 * @Author kongyin
 * @Date 2020/7/23 14:11
 **/
//@Component
public class Backend extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        // 第一条路线生成一些订单并将其排队在数据库中
        from("timer:new-order?delay=1s&period={{quickstart.generateOrderPeriod:2s}}")
                .routeId("generate-order")
                .bean("orderService", "generateOrder")
                .to("sql:insert into orders (id, item, amount, description, processed) values " +
                        "(:#${body.id} , :#${body.item}, :#${body.amount}, :#${body.description}, false)?" +
                        "dataSource=#dataSource")
                .log("Inserted new order ${body.id}");

        // 第二条路线轮询数据库以获取新订单并对其进行处理
        from("sql:select * from orders where processed = false?" +
                "consumer.onConsume=update orders set processed = true where id = :#id&" +
                "consumer.delay={{quickstart.processOrderPeriod:5s}}&" +
                "dataSource=#dataSource")
                .routeId("process-order")
                .bean("orderService", "rowToOrder")
                .log("Processed order #id ${body.id} with ${body.amount} copies of the «${body.description}» book");
    }
}
