/*drop table if exists orders;

create table orders (
  id integer primary key,
  item varchar(10),
  amount integer,
  description varchar(30),
  processed boolean
);*/
drop table orders;
CREATE TABLE [dbo].[orders] (
  [id] int NOT NULL,
  [item] varchar(10) NULL,
  [amount] int NULL,
  [description] varchar(255) NULL,
  [processed] varchar(25) NULL,
  PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)