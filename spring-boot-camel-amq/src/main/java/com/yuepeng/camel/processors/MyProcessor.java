package com.yuepeng.camel.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MyProcessor implements Processor {
    /**
     *处理器是一个简单的Java接口，用于将自定义集成逻辑添加到路由。
     * 它包含一个用于对消费者接收到的消息执行定制业务逻辑的单一处理方法
     * 交换是消息的容器，当消费者在接收消息期间收到消息时创建交换
     * 路由过程。交换允许系统之间进行不同类型的交互-它可以定义
     * 单向消息或请求-响应消息
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String payload = exchange.getIn().getBody(String.class);
        System.out.println("payload: " + payload);
        // 在此处做一些有效载荷和/或交换
        exchange.getIn().setBody("Changed body at " + LocalDateTime.now());
    }
}