package com.yuepeng.camel.services;

import com.yuepeng.camel.exceptions.CustomRuntimeException;
import com.yuepeng.camel.pojos.Book;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MyService {
    public String doSomething(String body, Map headers, String jmsCorrelationIDparam) {
        // 处理体内并返回您想要的任何东西
        System.out.println("MyService doSomething body " + body);
        System.out.println("MyService doSomething jmsCorrelationIDparam " + jmsCorrelationIDparam);
        headers.forEach((k, v) -> System.out.println(k + ":" + v));
        System.out.println("Long process ...");
        // 停一段时间 ...
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //test
        if (body.contains("error")) {
            throw new CustomRuntimeException("MY ERROR !");
        }
        return "Bye World";
    }

    public String doSomethingBis(String body) {
        System.out.println("MyService doSomethingBis body " + body);
        return body;
    }

    public Book doSomethingJson(Book book) {
        //test
        if (book.getId() == 1) {
            throw new CustomRuntimeException("MY ERROR !");
        }
        //----
        System.out.println("MyService doSomethingJson body " + book);
        return book;
    }


    public List<Book> doSomethingJsonList(List<Book> books) {
        System.out.println("MyService doSomethingJsonList body " + books);
        return books;
    }

}
