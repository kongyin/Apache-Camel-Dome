package com.yuepeng.camel;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class SpringBootAndApacheCamelApplication {

    // 有关其他配置，请参见 https://camel.apache.org/components/3.0.x/spring-boot.html
    public static void main(String[] args) {
        SpringApplication.run(SpringBootAndApacheCamelApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            System.out.println("让我们检查一下Spring Boot提供的bean:");
            // 例如，我们可以看到与Apache骆驼一起使用的'dataSource'bean ...
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }
        };
    }
}
