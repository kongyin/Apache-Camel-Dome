package com.yuepeng.camel.routes;

import com.yuepeng.camel.exceptions.CustomRuntimeException;
import com.yuepeng.camel.pojos.Book;
import com.yuepeng.camel.processors.MyPrepareProcessor;
import com.yuepeng.camel.processors.MyProcessorError;
import com.yuepeng.camel.services.MyService;
import lombok.RequiredArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OtherCamelRoutes extends RouteBuilder {

    // JSON数组
    JacksonDataFormat format = new ListJacksonDataFormat(Book.class);

    private final MyProcessorError myProcessorError;

    private final MyPrepareProcessor myPrepareProcessor;

    private final MyService myService;

    @Override
    public void configure() throws Exception {

        // 全局（针对每个RouteBuilder实例的Java DSL）
        // 将使用原始消息（正文和标头）
        // 可以添加多个异常，然后是errorHandler
        onException(CustomRuntimeException.class)
                .useOriginalMessage()
                .maximumRedeliveries(5).redeliveryDelay(1000);

        // 将使用原始消息（正文和标头）
        // see https://camel.apache.org/components/latest/eips/dead-letter-channel.html
        // and https://camel.apache.org/manual/latest/error-handler.html
        // 当“死信通道”正在重新交付时，可以配置一个在之前执行的处理器
        // 每次重新交付的尝试。这可用于您需要在重新传递消息之前对其进行更改的情况
        // 在将交换发送到死信队列之前，您可以使用onPrepare允许自定义处理器
        // 准备交换，例如添加有关交换失败原因的信息
        errorHandler(deadLetterChannel("{{input.dlq.name}}")
                .onRedelivery(myProcessorError)
                .onPrepareFailure(myPrepareProcessor)
                .useOriginalMessage().maximumRedeliveries(3).redeliveryDelay(2000));

        from("activemq:queue.testggal3")
                .bean(myService, "doSomething(${body}, ${headers}, ${headers.JMSCorrelationID})")
                .log("Camel body: ${body}");

        // see https://stackoverflow.com/questions/50732754/configuring-datasource-for-apache-camel
        // and https://camel.apache.org/components/latest/jdbc-component.html
        // 无需在骆驼注册表中注册我们的数据源?
        from("timer:mytimer2?period=10000&delay=25000&repeatCount=3")
                .setBody(constant("SELECT * FROM BOOK"))
                .to("jdbc:dataSource")
                .split(body())
                .bean(myService, "doSomethingBis(${body})")
                .log("Camel body from JDBC and bean: ${body}");


        // JSON https://camel.apache.org/manual/latest/json.html
        // and https://stackoverflow.com/questions/40756027/apache-camel-json-marshalling-to-pojo-java-bean
        // and https://stackoverflow.com/questions/46411214/how-to-unmarshal-json-body-to-list-of-myclass-in-camel/46411822
        // see https://camel.apache.org/components/latest/seda-component.html
        // JSON : {"id": 1, "title": "LOTR 1", "author": "toto"}
        from("activemq:queue.testggal4")
                .wireTap("seda:audit")
                .unmarshal().json(JsonLibrary.Jackson, Book.class)
                .bean(myService, "doSomethingJson(${body})")
                .log("Camel body unmarshal and bean: ${body}");

        // with JSON :
        /*
            [{
            "id": 1,
            "title": "LOTR 1",
            "author": "toto"
            },
            {
                "id": 2,
                "title": "LOTR 2",
                "author": "toto"
            }
            ]
         */
        // 直接路由其他路线
        // https://camel.apache.org/components/latest/eips/split-eip.html
        from("activemq:queue.testggal5")
                .unmarshal(format)
                .bean(myService, "doSomethingJsonList(${body})")
                .split(body())
                .marshal().json()
                .to("direct:customLog");


        from("activemq:queue.testggal6?concurrentConsumers=2")
                .process(exchange -> {
                    System.out.println(Thread.currentThread() + " - " + exchange.getIn().getBody());
                    Thread.sleep(5000);
                });

    }
}
