package com.yuepeng.camel.routes;

import com.yuepeng.camel.processors.MyProcessor;
import lombok.RequiredArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CamelRoutes extends RouteBuilder {

    private final MyProcessor myProcessor;

    /**
     * 路由将自动启动 可以在application.properties中自定义Camel应用程序
     * https://camel.apache.org/manual/latest/dsl.html
     * JAVA DSL此处 https://camel.apache.org/manual/latest/java-dsl.html
     * Camel 注解 DSL  https://camel.apache.org/manual/latest/bean-integration.html
     * @throws Exception
     */
    @Override
    public void configure() throws Exception {
        /**
         * 使用Apache ActiveMQ 5.15.12（要启动代理：activemq start）
         * ActiveMQ管理界面位于 http://127.0.0.1:8161/admin/（admin / admin）
         * Broker  tcp://localhost:61616
         */


        /**
         * direct 组件 https://camel.apache.org/components/latest/direct-component.html
         */
        from("direct:firstRoute")
                .log("Camel body: ${body}");


        /**
         * timer 组件 https://camel.apache.org/components/latest/timer-component.html
         * activemq 组件 https://camel.apache.org/components/latest/activemq-component.html
         * 简单语法 https://camel.apache.org/components/latest/languages/simple-language.html
         */
        from("timer:mytimer?period=5000&delay=20000&repeatCount=3")
                .setBody(simple("Hello from timer at ${header.firedTime}"))
                .to("activemq:queue.testggal");


        /**
         * activemq
         * https://camel.apache.org/manual/latest/processor.html
         */
        from("activemq:queue.testggal")
                .process(myProcessor)
                .log("Camel body from jms: ${body}")
                .to("activemq:queue.testggal2");

        from("direct:customLog")
                .log("Camel body from customLog : ${body}");
    }
}
